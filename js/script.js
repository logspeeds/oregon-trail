function Dog (name, breed, isGoodBoy) {
    this.name = name;
    this.breed = breed;
    this.isGoodBoy = isGoodBoy;
 }


 
 Dog.prototype = {
    constructor: Dog,
    sit: function () {
        // sitting code here
    },
    fetch: function () {
        // fetching code here
    }
 }

 function Traveler (name, food, isHealthy){
     this.name = name;
     this.food = 1;
     this.isHealthy = true;
 }


function Wagon (capacity,passengers){
    this.capacity = capacity;
    this.passengers = [];
}

Traveler.prototype = {
    constructor: Traveler,

    //Aumenta a comida do viajante em 2.
    hunt: function () {
        this.food += 2;
    },

    /*Consome 1 unidade da comida do viajante. Se o viajante não tiver comida
     para comer, ele deixa de estar saudável.*/
    eat: function () {
        this.food -= 1;
        if (this.food < 1) {
            this.isHealthy = false;
        }
    }
}

Wagon.prototype = {
    constructor: Wagon,

    /*Retorna o número de assentos vazios, determinado pela capacidade que
     foi estipulada quando a carroça foi criada comparado com o número de 
    passageiros a bordo no momento.*/
    getAvailableSeatCount: function () {
        let availableSeatCount = "Há " + this.capacity + " assentos vazios na carroça";
        return availableSeatCount
    },

    /*Adicione o viajante à carroça se tiver espaço. Se a carroça já estiver 
    cheia, não o adicione.*/
    join: function (Traveler) {
        if (this.capacity > 0) {
            this.capacity -= 1;
            this.passengers.push(Traveler);

        } 
        else {
            let noAvailableSeatCount = "Não há assentos vazios na carroça";
            return noAvailableSeatCount
            
        }
    },

    /*Retorna true se houver pelo menos uma pessoa não saudável na carroça. 
    Retorna false se não houver.*/
    shouldQuarantine: function () {
        let quarantine = false;
        for(i = 0; i < this.passengers.length; i++) {
            
            if (this.passengers[i]["isHealthy"] == true) {
                quarantine = false
            }
            else {
                quarantine = true;
                i = this.passengers.length
            }


        }
        return quarantine
    },

    /*Retorna true se houver pelo menos uma pessoa não saudável na carroça. 
    Retorna false se não houver.*/
    totalFood: function () {
        let allFood = 0;
        for(i = 0; i < this.passengers.length; i++) {
            
            if (this.passengers[i]["food"] > 0) {
                allFood += this.passengers[i]["food"];
            }
        }
        return allFood
    },
}